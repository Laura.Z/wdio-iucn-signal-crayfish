describe('iucn search', () => {

    it('should open main url', async () => {
        await browser.url('https://www.iucnredlist.org/')
    })

    it('should click search bar', async () => {
        const searchBar = await $('.search');
        await searchBar.click();
    })

    it('should type signal crayfish', async () =>{
        const input = await $('.search');
        await input.setValue ('Signal crayfish');
    })

    it('should click site button key ', async () => {
        const siteButtonKey = await $('.search--site__button__key');
        await siteButtonKey.click();
    })
    
    it('should click signal crayfish box', async () => {
        const box = await $('.link--faux');
        await box.click();
    })

    it('should click conservation evidence link', async () => {
        const link = await $('//*[@id="conservationevidence"]/div[1]/div/p/strong/a');
        await link.click();
    })

})